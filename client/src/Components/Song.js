import React, { Component } from 'react';
import {ListGroupItem} from 'react-bootstrap';

export default class Song extends Component{
	constructor(props){
		super(props);
		this.state = {
			song: props.song
		};
	};

	render(){
		var song = this.state.song;

		return (
			<ListGroupItem header={song.title}>
				Artist: {song.artist}<br/>
				Album: {song.album}<br/>
				Duration: {song.duration}
			</ListGroupItem>
		);
	};
}