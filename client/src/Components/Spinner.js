import React, { Component } from 'react';
import ReactLoading from 'react-loading';

export default class Spinner extends Component{
	render(){
		if(this.props.isLoading){
			return (
				<ReactLoading
					style={{
						margin:'auto',
						color:'#000000',
						height:'64px',
						width:'64px'
					}}
					delay={0} 
					type='bars'/>
			);
		}

		return <div/>
	}
}