import React, { Component } from 'react';
import Spinner from './Spinner';
import Song from './Song';
import {ListGroup, PageHeader} from 'react-bootstrap';

export default class Playlist extends Component{
	constructor(props){
		super(props);
		this.state = {
			playlist: null,
			songs: [],
			isLoading: true
		};
	};

	async componentDidMount(){
		var playlist = await this.props.apiClient.getPlayListAsync(this.props.id);
		var songs = await this.props.apiClient.getSongListAsync(playlist.songs);

		this.setState({
			playlist:playlist,
			songs: songs,
			isLoading:false
		});
	}

	render(){
		if(this.state.isLoading){
			return <Spinner isLoading={this.state.isLoading}/>
		}
		else{
			var playlist = this.state.playlist;
			return(
				<div>
					<PageHeader>{playlist.name}</PageHeader>
					{this.renderSongs()}
				</div>
			);
		}
	};

	renderSongs(){
		return (
			<ListGroup>
				{this.state.songs.map(s => <Song key={s.id} song={s}/>)}
			</ListGroup>
		);
	};
} 