import React, { Component } from 'react';
import {ListGroup} from 'react-bootstrap';
import Song from './Song';
import Spinner from './Spinner';

export default class Library extends Component {
	constructor(props){
		super(props);

		this.state = {
			songs:[],
			isLoading:true
		}
	}

	async componentDidMount(){
		var songs = await this.props.apiClient.getSongsAsync();
		this.setState({
			songs:songs,
			isLoading:false
		});
	}

	render(){
		return (
			<div>
				<h1>Library</h1>
				<Spinner isLoading={this.state.isLoading}/>
				<div>{this.renderSongs()}</div>
			</div>
		);
	};

	renderSongs(){
		return (
			<ListGroup>
				{this.state.songs.map(s => <Song key={s.id} song={s}/>)}
			</ListGroup>
		);
	};
} 