import React, { Component } from 'react';
import {ListGroup, ListGroupItem} from 'react-bootstrap';
import Spinner from './Spinner';

class PlaylistItem extends Component{
	constructor(props){
		super(props);
		this.state = {
			playlist: props.playlist
		};
	};

	render(){
		var playlist = this.state.playlist;

		return (
			<ListGroupItem header={playlist.name} href={`/playlists/${playlist.id}`}>
				Songs: {playlist.songs.length}
			</ListGroupItem>
		);
	};
}

export default class Playlists extends Component {
	constructor(props){
		super(props);

		this.state = {
			playlists:[],
			isLoading:true
		}
	}

	async componentDidMount(){
		var playlists = await this.props.apiClient.getPlaylistsAsync();
		this.setState({
			playlists:playlists,
			isLoading:false
		});
	}

	render(){
		return (
			<div>
				<h1>Playlists</h1>
				<Spinner isLoading={this.state.isLoading}/>
				<div>{this.renderPlaylists()}</div>
			</div>
		);
	};

	renderPlaylists(){
		return (
			<ListGroup>
				{this.state.playlists.map(pl => <PlaylistItem key={pl.id} playlist={pl}/>)}
			</ListGroup>
		);
	};
} 