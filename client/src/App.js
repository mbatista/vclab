import './App.css';
import React, { Component } from 'react';
import {Grid, Row, Nav, Navbar, NavItem, PageHeader} from 'react-bootstrap';
import Library from './Components/Library';
import Playlists from './Components/Playlists';
import Playlist from './Components/Playlist';
import Home from './Components/Home';
import * as ApiClient from './ApiClient';

import { BrowserRouter as Router, Route } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Grid className="App">
        <Row>
          <PageHeader>
            VC Lab Challenge
          </PageHeader>
        </Row>
        <Row className="show-grid">
            <Navigation/>
        </Row>
        <Row>
          <Routes/>
        </Row>
      </Grid>
    );
  }
}

class Navigation extends Component {
  render(){
    return(
      <Navbar>
        <Nav activeKey={1}>
          <NavItem href="/">Home</NavItem>
          <NavItem href="library">Library</NavItem>
          <NavItem href="playlists">Playlists</NavItem>
        </Nav>
      </Navbar>
    );
  }
}

const Routes = () => (
  <Router>
    <div>
      <Route exact path="/" component={Home}/>
      <Route exact path="/library" component={() => <Library apiClient={ApiClient}/>}/>
      <Route exact path="/playlists" component={() => <Playlists apiClient={ApiClient}/>}/>
      <Route path="/playlists/:id" component={({ match }) => <Playlist id={match.params.id} apiClient={ApiClient}/>}/>
    </div>
  </Router>
)

export default App;
