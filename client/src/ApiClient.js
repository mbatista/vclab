const baseUrl = "http://localhost:5000"

export async function getSongsAsync(){
    var url = `${baseUrl}/Library/`;
    var res = await fetch(url);
    return await res.json();
}

export async function getSongAsync(id){
	var url = `${baseUrl}/Library/${id}/`;
	var res = await fetch(url);
	return await res.json();
}

export async function getSongListAsync(ids){
	var promises = ids.map(id => getSongAsync(id));
	return Promise.all(promises);
}

export async function getPlaylistsAsync(){
  var url = `${baseUrl}/Playlist/`
  var res = await fetch(url);
  return await res.json();
}

export async function getPlayListAsync(id){
	var url = `${baseUrl}/Playlist/${id}/`;
	var res = await fetch(url);
	return await res.json();
}